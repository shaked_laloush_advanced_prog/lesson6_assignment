﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Calendar : Form
    {
        string BDName;
        public Calendar(string name)
        {
            //gets the name of the file
            BDName = name;
            InitializeComponent();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {

            string line;
            bool Flag = false;

            
            //checks if file exists if it doesn't creates new file
            if (!File.Exists(@"C:\Users\magshimim\Desktop\Files\" + BDName + "BD.txt"))
            {
                FileStream fs = new FileStream(@"C:\Users\magshimim\Desktop\Files\"+BDName+"BD.txt", FileMode.CreateNew);
            }
            //if file does exist read from it
            else
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(@"C:\Users\magshimim\Desktop\Files\" + BDName + "BD.txt");
                //reads line by line
                while ((line = file.ReadLine()) != null)
                {
                    //gets the month and day + name of BDperson
                    int indexofmonth = line.IndexOf(',');
                    int indexofday = line.IndexOf('/');
                    string day = line.Remove(0, indexofday + 1);
                    day = day.Substring(0, day.LastIndexOf("/"));
                    string month = line.Remove(0, indexofmonth + 1);
                    month = month.Substring(0, month.LastIndexOf("/"));
                    month = month.Substring(0, month.LastIndexOf("/"));
                    string name = line.Substring(0, line.LastIndexOf(","));
                    //checks for every date that is in DB if it is the chosen one
                    if (e.End.Date.Month == Int32.Parse(month) && e.End.Date.Day == Int32.Parse(day))
                    {
                        Date_things.Text = "In This Date- it's " + name + "'s Birthday";
                        Flag = true;
                    }
                    
                }
                //closes file
                file.Close();
            }
            //if flag is false that means nothing happens in this date
            if(Flag==false)
            {
                Date_things.Text = "In This Date-Nothing happens";
            }
           

            
            
        }
    }
}
