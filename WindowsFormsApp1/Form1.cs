﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Password_Text_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //flag for if the connection didn't work  
            bool flag = false;
            string line;

            // get file  
            System.IO.StreamReader file =
            new System.IO.StreamReader(@"C:\Users\magshimim\Desktop\Files\Users.txt");
            //read line by line the file
            while ((line = file.ReadLine()) != null)
            {
                //gets the index of the first ,
                int index = line.IndexOf(',');
                //gets the pass
                string pass= line.Remove(0, index+1);
                //gets the name of the username
                string name = line.Substring(0,line.LastIndexOf(","));
                //checks if name and password are ok
                if (Name_Enter.Text.Equals(name) && Password_Enter.Text.Equals(pass))
                {
                    //creates new calendar
                    Calendar Cal = new Calendar(name);
                    //resets values so they will not be tracked
                    Name_Enter.Text = "";
                    Password_Enter.Text = "";
                    MessageBox.Text = "";
                    //hide current window
                    this.Hide();
                    Cal.ShowDialog();
                    file.Close();
                    //shows window
                    this.Show();
                    //flag is true and break
                    flag = true;
                    break;
                }
            }
            //if flag is false that means wrong password or wrong username
            if(flag==false)
            {
                MessageBox.Text = "Wrong Username or Password Please Try Again!";
            }
            
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //closes all programs
            this.Hide();
            this.Close();

        }

        private void Name_Enter_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
