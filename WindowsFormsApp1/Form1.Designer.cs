﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Name_Enter = new System.Windows.Forms.TextBox();
            this.Password_Enter = new System.Windows.Forms.TextBox();
            this.UserName_Text = new System.Windows.Forms.Label();
            this.Pass_Text = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.MessageBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Name_Enter
            // 
            this.Name_Enter.Location = new System.Drawing.Point(80, 76);
            this.Name_Enter.Name = "Name_Enter";
            this.Name_Enter.Size = new System.Drawing.Size(128, 20);
            this.Name_Enter.TabIndex = 1;
            this.Name_Enter.TextChanged += new System.EventHandler(this.Name_Enter_TextChanged);
            // 
            // Password_Enter
            // 
            this.Password_Enter.Location = new System.Drawing.Point(80, 105);
            this.Password_Enter.Name = "Password_Enter";
            this.Password_Enter.PasswordChar = '*';
            this.Password_Enter.Size = new System.Drawing.Size(128, 20);
            this.Password_Enter.TabIndex = 2;
            this.Password_Enter.TextChanged += new System.EventHandler(this.Password_Text_TextChanged);
            // 
            // UserName_Text
            // 
            this.UserName_Text.AutoSize = true;
            this.UserName_Text.Location = new System.Drawing.Point(14, 79);
            this.UserName_Text.Name = "UserName_Text";
            this.UserName_Text.Size = new System.Drawing.Size(60, 13);
            this.UserName_Text.TabIndex = 3;
            this.UserName_Text.Text = "UserName:";
            this.UserName_Text.Click += new System.EventHandler(this.label1_Click);
            // 
            // Pass_Text
            // 
            this.Pass_Text.AutoSize = true;
            this.Pass_Text.Location = new System.Drawing.Point(14, 105);
            this.Pass_Text.Name = "Pass_Text";
            this.Pass_Text.Size = new System.Drawing.Size(56, 13);
            this.Pass_Text.TabIndex = 4;
            this.Pass_Text.Text = "Password:";
            this.Pass_Text.Click += new System.EventHandler(this.label2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 217);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 32);
            this.button1.TabIndex = 5;
            this.button1.Text = "Enter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(105, 217);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 32);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MessageBox
            // 
            this.MessageBox.BackColor = System.Drawing.SystemColors.MenuBar;
            this.MessageBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MessageBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBox.ForeColor = System.Drawing.Color.Red;
            this.MessageBox.Location = new System.Drawing.Point(0, 141);
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.Size = new System.Drawing.Size(282, 13);
            this.MessageBox.TabIndex = 7;
            this.MessageBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 274);
            this.Controls.Add(this.MessageBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Pass_Text);
            this.Controls.Add(this.UserName_Text);
            this.Controls.Add(this.Password_Enter);
            this.Controls.Add(this.Name_Enter);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Name_Enter;
        private System.Windows.Forms.TextBox Password_Enter;
        private System.Windows.Forms.Label UserName_Text;
        private System.Windows.Forms.Label Pass_Text;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox MessageBox;
    }
}

